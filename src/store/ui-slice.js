import { createSlice } from '@reduxjs/toolkit';

const uiInitialState = {
    showCart: true,
    notification: null
}

const uiSlice = createSlice({
    name: 'ui',
    initialState: uiInitialState,
    reducers: {
        toggleCartShow: state => { state.showCart = !state.showCart },
        showCart: state => { state.showCart = true },
        hideCart: state => { state.showCart = false },
        showNotification(state, action) {
            if (action.payload) {
                state.notification = {
                    status: action.payload.status,
                    title: action.payload.title,
                    message: action.payload.message
                }
            } else {
                state.notification = null;
            }
        }
    }
});

export const uiActions = uiSlice.actions;

export default uiSlice.reducer;