import { uiActions } from "./ui-slice";
import { cartActions } from "./cart-slice";
import { showTimedNotification } from "./ui-actions";

// This is action creator thunk
export const sendCartData = (cart) => {
    return async (dispatch) => {
        dispatch(uiActions.showNotification({
            status: 'pending',
            title: 'Sending...',
            message: 'Sending cart data'
        }));

        const sendRequest = async () => {
            const response = await fetch('https://react-redux-app-344e0-default-rtdb.europe-west1.firebasedatabase.app/cart.json', {
                method: 'PUT',
                body: JSON.stringify({
                    items: cart.items,
                    itemsAmount: cart.itemsAmount,
                    totalPrice: cart.totalPrice
                })
            });

            if (!response.ok) {
                throw new Error('Sending cart data failed')
            }
        };

        try {
            await sendRequest();
            dispatch(showTimedNotification({
                status: 'success',
                title: 'Success!',
                message: 'Sent cart data successfully'
            }));
        } catch (error) {
            dispatch(uiActions.showNotification({
                status: 'error',
                title: 'Error!',
                message: 'Sending cart data failed'
            }));
        }
    };
};

export const getCartData = () => {
    return async (dispatch) => {
        const fetchData = async () => {
            const response = await fetch('https://react-redux-app-344e0-default-rtdb.europe-west1.firebasedatabase.app/cart.json');
            if (!response.ok) {
                throw new Error('Retrieving cart data failed');
            }
            const data = await response.json();
            return data;
        };

        try {
            const cartData = await fetchData();
            const cartDataToDispatch = {
                items: (cartData === null || cartData.items === undefined) ? [] : cartData.items,
                itemsAmount: cartData !== null ? cartData.itemsAmount : 0,
                totalPrice: cartData !== null ? cartData.totalPrice : 0
            }
            dispatch(cartActions.replaceCart(cartDataToDispatch));
        } catch (error) {
            dispatch(uiActions.showNotification({
                status: 'error',
                title: 'Error!',
                message: 'Retrieving cart data failed'
            }));
        }
    };
};
