import { uiActions } from "./ui-slice";

export const showTimedNotification = (notificationData) => {
    return async (dispatch) => {
        dispatch(uiActions.showNotification(notificationData));
        await setTimeout(() => {
            dispatch(uiActions.showNotification(null));
        }, 5000);
    };
};