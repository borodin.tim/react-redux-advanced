import { createSlice } from '@reduxjs/toolkit';

const cartInitialState = {
    items: [],
    itemsAmount: 0,
    totalPrice: 0,
    changed: false
};

const cartSlice = createSlice({
    name: 'cart',
    initialState: cartInitialState,
    reducers: {
        addItemToCart(state, action) {
            const newItem = action.payload;
            const idx = state.items.findIndex(item => item.id === newItem.id);
            if (idx >= 0) {
                // It is Ok, since we're using Redux Toolkit!
                state.items[idx].quantity += 1;
            } else {
                // It is Ok, since we're using Redux Toolkit!
                state.items.push(newItem);
            }
            state.itemsAmount++;
            state.totalPrice += newItem.price;
            state.changed = true;
        },
        subtractItemFromCart(state, action) {
            const idToSubtract = action.payload;
            const idx = state.items.findIndex(item => item.id === idToSubtract);

            const itemToSubtractPrice = state.items[idx].price;

            if (idx >= 0) {
                if (state.items[idx].quantity > 1) {
                    // It is Ok, since we're using Redux Toolkit!
                    state.items[idx].quantity -= 1;
                } else {
                    // It is Ok, since we're using Redux Toolkit!
                    const newItems = state.items.filter(item => item.id !== idToSubtract);
                    state.items = newItems;
                }
            }
            state.itemsAmount--;
            state.totalPrice -= itemToSubtractPrice;
            state.changed = true;
        },
        handleDeleteFromCart(state, action) {
            const idToDelete = action.payload;

            const idx = state.items.findIndex(item => item.id === idToDelete);
            const itemToDelete = state.items[idx];

            const newItems = state.items.filter(item => item.id !== idToDelete);
            state.items = newItems;
            state.itemsAmount -= itemToDelete.quantity;
            state.totalPrice -= itemToDelete.price * itemToDelete.quantity;
            state.changed = true;
        },
        replaceCart(state, action) {
            state.items = action.payload.items;
            state.itemsAmount = action.payload.itemsAmount;
            state.totalPrice = action.payload.totalPrice;
            state.changed = false;
        }
    }
});

export const cartActions = cartSlice.actions;

export default cartSlice.reducer;