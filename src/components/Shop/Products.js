import ProductItem from './ProductItem';
import classes from './Products.module.css';

const DUMMY_PRODUCTS = [
  {
    // id: Math.floor((1 + Math.random()) * 0x10000),
    id: 1,
    title: 'Test Item',
    price: 6,
    description: 'This is a first product - amazing!',
  },
  {
    id: 2,
    title: 'Computer Keyboard',
    price: 110,
    description: 'Mechanical, clicky!',
  },
  {
    id: 3,
    title: 'Computer Mouse',
    price: 70,
    description: 'Logitec',
  },
  {
    id: 4,
    title: 'Display Screen',
    price: 90,
    description: '4k',
  },
]

const Products = (props) => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {DUMMY_PRODUCTS.map(product => (
          <ProductItem
            key={product.id}
            title={product.title}
            price={product.price}
            description={product.description}
            id={product.id}
          />
        ))}

      </ul>
    </section>
  );
};

export default Products;
