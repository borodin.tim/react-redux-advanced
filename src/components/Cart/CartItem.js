import { useDispatch } from 'react-redux';

import classes from './CartItem.module.css';
import { cartActions } from '../../store/cart-slice';

const CartItem = (props) => {
  const { title, quantity, total, price, id } = props.item;

  const dispatch = useDispatch();

  const handleSubtractItemFromCart = () => {
    dispatch(cartActions.subtractItemFromCart(id));
  }

  const handleAddItemToCart = () => {
    dispatch(cartActions.addItemToCart({
      id,
      title,
      quantity: 1,
      price
    }));
  }

  const handleDeleteFromCart = () => {
    dispatch(cartActions.handleDeleteFromCart(id));
  }

  return (
    <li className={classes.item}>
      <header>
        <h3>{title}</h3>
        <div className={classes.price}>
          ${total.toFixed(2)}{' '}
          <span className={classes.itemprice}>(${price.toFixed(2)}/item)</span>
        </div>
      </header>
      <div className={classes.details}>
        <div className={classes.quantity}>
          x <span>{quantity}</span>
        </div>
        <div className={classes.actions}>
          <button type="button" onClick={handleSubtractItemFromCart}>-</button>
          <button type="button" onClick={handleAddItemToCart}>+</button>
          <button
            type="button"
            alt="Delete"
            className={classes['delete-btn']}
            onClick={handleDeleteFromCart}>X</button>
        </div>
      </div>
    </li>
  );
};

export default CartItem;
