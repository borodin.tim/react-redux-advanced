import { useSelector } from 'react-redux';

import Card from '../UI/Card';
import classes from './Cart.module.css';
import CartItem from './CartItem';

const Cart = (props) => {
  const cartItems = useSelector(state => state.cart.items);
  const cartTotalPrice = useSelector(state => state.cart.totalPrice);

  return (
    <Card className={classes.cart}>
      {cartItems.length > 0 && <div>
        <h2>Your Shopping Cart</h2>
        <ul>
          {cartItems.map(item => {
            return (
              <CartItem
                key={item.id}
                item={
                  {
                    ...item,
                    total: item.price * item.quantity
                  }
                } />
            );
          })}
        </ul>
        <h3 className={classes.total}>
          <span>Total</span>
          <span>${cartTotalPrice}</span>
        </h3>
      </div>}
      {
        cartItems.length === 0 &&
        <div>
          <h2 className={classes['empty-cart-msg']}>Cart is Empty</h2>
        </div>
      }
    </Card>
  );
};

export default Cart;
